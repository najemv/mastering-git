As I am making this HW 03 after attending class 4,
I can proudly say that thanks to the last lecture I didn't lose all the work I was working on for the past 2 days.
At my job, I was fixing a rather complicated bug on my FE MR.
It was late in the evening and I push the fix and wanted to rebase (I didn't really need to be rebased manually,
but I wanted to be the nice guy).
However, only when pushed -f, I realized I rebased to the wrong branch (It was bug on RELease and I rebase DEV).
Without the confidence from the lecture in what the rebase -i is and all its power, I would probably have to create new branch and copy the changes by hand.

Thank you many many times for this course
